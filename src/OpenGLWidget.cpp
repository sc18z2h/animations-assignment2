/*
===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
*/

#include <fstream>
#include <sstream>

#include <Qt>
#include <QtMath>
#include <QFileDialog>
#include <QQuaternion>
#include <QTimer>
#include <QMatrix4x4>
#include <QApplication>
#include <QStringBuilder>
#include <QDebug>

#include "include/gif.h"

#include "OpenGLWidget.h"
#include "AdaptiveOpenGL.h"

using namespace std;

const double nearZ = 0.1, farZ = 100.0;
constexpr unsigned int MAX_RECORDING_FRAMES = 1024;

OpenGLWidget::OpenGLWidget(QWidget* parent)
	:
	QGLWidget(parent),
	isVerticesDraggable(false),
	isSimulationPlaying(false),
	timer(this),
	cameraPosition(0.0f, 0.0f, 0.0f),
	scale(20.0f),
	lastMouseX(0),
	lastMouseY(0),
	lastElapsedTime(0),
	draggedVertexIndex(-1),
	isDraggedVertexFixed(false),
	sphereQuadratic(nullptr),
	sphereRotationAngle(0.0f),
	isRecording(false),
	recordWidth(0),
	recordHeight(0),
	recordFrame(0),
	numberOfFramesToSkip(5),
	framesToSkipLeft(0)
{
	// make controls focus on widget when widget is pressed
	setFocusPolicy(Qt::FocusPolicy::StrongFocus);
	// we want keyboard events for ourselves
	grabKeyboard();
	// update drawing over time
	connect(&timer, &QTimer::timeout, this, &OpenGLWidget::updateGL);
	timer.start();
	// elasedTimer used for deltaTime calculation
	elapsedTimer.start();
	// update last time, to improve delta time accuracy
	lastElapsedTime = elapsedTimer.elapsed();
	// get mouse move events even without mouse buttons being pressed
	setMouseTracking(true);
	sphereQuadratic = gluNewQuadric();
}

OpenGLWidget::~OpenGLWidget()
{
	gluDeleteQuadric(sphereQuadratic);
}

void OpenGLWidget::LoadWavefrontModel()
{
	const char* defaultFolder = "models/";
	if (QDir(defaultFolder).exists() == false)
		// if folder doesn't exist, 
		defaultFolder = "";

	string modelFileName = QFileDialog::getOpenFileName
	(
		this,
		"Load 3D model",
		defaultFolder,
		"Wavefront (*.obj);; All Files (*.*)",
		nullptr,
		// avoids problems on mac and linux
		QFileDialog::DontUseNativeDialog
	).toStdString();
	ifstream modelFileStream(modelFileName);

	if (modelFileStream.is_open())
	{
		mesh.Clear();
		mesh.ReadFromWavefront(modelFileStream);
		// move the mesh up in the air
		mesh.MoveCentreOfMassTo(QVector3D(0, 2, 0));

		cloth.RepresentMesh(&mesh);

		OnMeshChanged(mesh);
	}
	else
	{
		OnMeshChanged(mesh);
		qWarning() << "Could not open 3D model file: " + QString(modelFileName.c_str());
	}
}

void OpenGLWidget::SaveWavefrontModel()
{
	const char* defaultFolder = "models/";
	if (QDir(defaultFolder).exists() == false)
		// if folder doesn't exist, 
		defaultFolder = "";

	string modelFileName = QFileDialog::getSaveFileName
	(
		this,
		"Save 3D model",
		defaultFolder,
		"Wavefront (*.obj);; All Files (*.*)"
	).toStdString();
	ofstream modelFileStream(modelFileName);

	mesh.WriteToWavefront(modelFileStream);
}

void OpenGLWidget::LoadTextureImage()
{
	const char* defaultFolder = "textures/";
	if (QDir(defaultFolder).exists() == false)
		// if folder doesn't exist, 
		defaultFolder = "";

	QString textureFileName = QFileDialog::getOpenFileName
	(
		this,
		"Load texture image",
		defaultFolder,
		"Images (*.bmp *.gif *.jpg *.jpeg *.png *.pbm *.pgm *.ppm *.xbm *.xpm);; All Files (*.*)",
		nullptr,
		// avoids problems on mac and linux
		QFileDialog::DontUseNativeDialog
	);

	if (mesh.ReadFromTexture(textureFileName))
	{
		mesh.isTextureEnabled = true;
		OnIsTextureEnabledChanged(true);
	}
	else
	{
		qWarning() << "Could not open texture image file: " + textureFileName;
	}
}

void OpenGLWidget::StepSimulation()
{
	// pause simulation, otherwise you can't really see 1 step
	isSimulationPlaying = false;
	cloth.Update(0.01);
	// rotate collider sphere
	sphereRotationAngle += cloth.collideSphereAngularVelocity * 0.01;
}

void OpenGLWidget::ToggleRecord()
{
	if (isRecording)
	{
		bool success = false;
		GifWriter gifWriter;
		string fileName = GetRecordingFileName().toStdString();
		
		if (GifBegin(
			&gifWriter, fileName.c_str(),
			recordWidth,
			recordHeight,
			// 1 indicates this is a gif animation,
			// and the animation header will be created
			1))
		{
			qDebug() << "Processing recording ...";

			size_t sizeOfFrame = GetSizeOfRecordingFrame();

			// flip the frames upside-down here
			// because OpenGL is doing it the other way
			{
				// used for swapping
				// remember *4 for each RGBA component
				vector<uint8_t> cacheRow((size_t)recordWidth * 4);
				auto recordBegin = recordData.begin();
				auto cacheRowBegin = cacheRow.begin();
				auto cacheRowLast = cacheRow.end() - 1;
				// iterate frames
				for (unsigned int i = 0; i < recordFrame; ++i)
				{
					// iterate half of the rows
					for (size_t y = 0; y < recordHeight / 2; ++y)
					{
						// copy the top row into cacheRow
						auto topRowBegin = recordBegin + i * sizeOfFrame + y * recordWidth * 4;
						auto topRowLast = recordBegin + i * sizeOfFrame + (y + 1) * recordWidth * 4 - 1;
						std::copy(topRowBegin, topRowLast, cacheRowBegin);

						// copy the bottom row into the top row
						size_t bottomRow = recordHeight - y - 1;
						auto bottomRowBegin = recordBegin + i * sizeOfFrame + bottomRow * recordWidth * 4;
						auto bottomRowLast = recordBegin + i * sizeOfFrame + (bottomRow + 1) * recordWidth * 4 - 1;
						std::copy(bottomRowBegin, bottomRowLast, topRowBegin);

						// copy cacheRow into bottom row
						std::copy(cacheRowBegin, cacheRowLast, bottomRowBegin);
					}
				}
			}

			// iterate frames
			for (unsigned int i = 0; i < recordFrame; ++i)
			{
				GifWriteFrame
				(
					&gifWriter,
					// start address of frame i
					&recordData[i * sizeOfFrame],
					recordWidth,
					recordHeight,
					recordDelays[i],
					8,
					false
				);
			}
			
			success = GifEnd(&gifWriter);
		}

		if (success)
			qDebug() << "Recording saved in 'recordings/'.";
		else
			qWarning() << "Couldn't save recording in '" << fileName.c_str() << "', does the folder exist?";

		recordData.clear();
		// force deallocate data, potentially a lot
		recordData.shrink_to_fit();
		recordDelays.clear();
	}
	else
	{
		// start new recording
		// set the recording to the current viewport size
		// even if viewport is resized in a middle of a recording
		// we still want to record the same dimension as the start
		recordWidth = width();
		recordHeight = height();

		recordData.clear();
		recordDelays.clear();

		// will be used to limit the size of recording
		recordFrame = 0;

		qDebug() << "Recording started ...";
	}

	// toggle the boolean
	isRecording = !isRecording;
}

void OpenGLWidget::initializeGL()
{
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_FLAT);
	// light 0 parameters
	glEnable(GL_LIGHT0);
	GLfloat lightPosition[4] = { 0.1f, 1.0f, 0.1f, 0.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);
	// restore normals to unit length after scale transformations
	glEnable(GL_RESCALE_NORMAL);
	// able to use glColor instead of glMaterial
	glEnable(GL_COLOR_MATERIAL);

	// background is greyish
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		
	// set the logger to the context that just been created
	logger.initialize();	
	// Stops the following logging message about unperformant pixel reading method for recording
	// QOpenGLDebugMessage("APISource", 131154, "Pixel-path performance warning: Pixel transfer is synchronized with 3D rendering.", "MediumSeverity", "PerformanceType")
	logger.disableMessages(
		QOpenGLDebugMessage::APISource,
		QOpenGLDebugMessage::PerformanceType,
		QOpenGLDebugMessage::MediumSeverity);
	// everytime a message is logged, print it on the console
	QObject::connect(
		&logger, &QOpenGLDebugLogger::messageLogged, 
		[](const QOpenGLDebugMessage& debugMessage)
		{
			qDebug() << debugMessage;
		});
	// start asynchronous action
	logger.startLogging();
}

void OpenGLWidget::resizeGL(int w, int h)
{
	// reset the viewport
	glViewport(0, 0, w, h);

	// set projection matrix to be glOrtho based window size
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// compute the aspect ratio of the widget
	double aspectRatio = static_cast<double>(w) / static_cast<double>(h);
	gluPerspective(70.0, aspectRatio, nearZ, farZ);

	// update last time, to improve delta time accuracy
	lastElapsedTime = elapsedTimer.elapsed();
}

// called every time the widget needs painting
void OpenGLWidget::paintGL()
{
	// current elapsed time recorded at start of frame, in milliseconds
	qint64 elapsedTime = elapsedTimer.elapsed();
	// convert to seconds
	// difference, used to temporal update functions
	double deltaTime = (elapsedTime - lastElapsedTime) / 1000.0;
	// deltaTime cannot be too large, otherwise things deviate
	deltaTime = min(0.02, deltaTime);

	// record current frame before glClear
	if (isRecording && 0 <= --framesToSkipLeft)
	{
		// reset skipping back to max
		framesToSkipLeft = numberOfFramesToSkip;

		// due to the simplicity of gif.h
		// the file sizes can be huge
		// so put a hard limit on size
		if (MAX_RECORDING_FRAMES < ++recordFrame)
			// recording has exceeded maximum size
			// abruptly end recording
			ToggleRecord();

		size_t sizeOfFrame = GetSizeOfRecordingFrame();
		// increase total size to the next frame's required size
		recordData.resize(recordFrame * sizeOfFrame, 0);

		// capture the current OpenGL screen
		glReadPixels
		(
			0, 0,
			recordWidth,
			recordHeight,
			GL_RGBA,
			// little endian order
			GL_UNSIGNED_INT_8_8_8_8_REV,
			// start address of the current frame
			&recordData[(recordFrame - 1) * sizeOfFrame]
		);

		// the recordData needs to flipping upside-down
		// but let's save the computation to later
				
		// and make sure delay more than 0
		uint32_t delay = max(1, (int)ceilf
		(
			// convert from seconds to hundredths of a second
			deltaTime * 100.0f
			// compensate for skipped frames
			* numberOfFramesToSkip
		));
		recordDelays.push_back(delay);
	}

	// clear color and depth because we render to color and need depth test
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// apply camera transformation to matrix stack
	QMatrix4x4 cameraMatrix;
	// TRS order
	cameraMatrix.translate(-cameraPosition.x(), -cameraPosition.y(),
		// weird application of z translation basically ensures eye position NDCS(0.0,0.0,0.0)
		// stays behind the rendered objects
		// which makes the lighting more consistent (i.e. doesn't brighten if zoom in)

		// camera offset		centre of frustum	 scale that zooms in/out
		(-cameraPosition.z() - (nearZ + farZ) / 2) / scale);
	cameraMatrix.rotate(GetCameraQuaternion());
	glMultMatrixf(cameraMatrix.data());

	// draw unlit objects
	glDisable(GL_LIGHTING);

	// draw x,y,z axes	
	glLineWidth(1.0f);
	glBegin(GL_LINES);
	glColor3f(1.0f, 0.2f, 0.2f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glColor3f(0.2f, 1.0f, 0.2f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	glColor3f(0.2f, 0.2f, 1.0f);
	glVertex3f(0.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 0.0f, -1.0f);
	glEnd();

	// only update if simulation is going
	if (isSimulationPlaying)
		cloth.Update(deltaTime);

	// draw light objects
	glEnable(GL_LIGHTING);

	// draw collide plane of cloth
	if (cloth.collideAgainst == Cloth::CollideAgainst::plane ||
		cloth.collideAgainst == Cloth::CollideAgainst::sphere)
	{
		glColor3f(0.2f, 0.2f, 0.2f);
		glBegin(GL_QUADS);

		glNormal3f(0.0f, 1.0f, 0.0f);

		// add small offset downwards to avoid z-fighting
		glVertex3f(-10.0f, cloth.collidePlaneOriginY - 0.1f, 10.0f);
		glVertex3f(10.0f, cloth.collidePlaneOriginY - 0.1f, 10.0f);
		glVertex3f(10.0f, cloth.collidePlaneOriginY - 0.1f, -10.0f);
		glVertex3f(-10.0f, cloth.collidePlaneOriginY - 0.1f, -10.0f);

		glEnd();
	}

	// draw collide sphere of cloth
	if (cloth.collideAgainst == Cloth::CollideAgainst::sphere)
	{
		if (isSimulationPlaying)
			sphereRotationAngle += cloth.collideSphereAngularVelocity * deltaTime;

		glPushMatrix();
		glColor3f(0.2f, 0.2f, 0.2f);
		glTranslatef(
			cloth.collideSphereOrigin[0],
			cloth.collideSphereOrigin[1],
			cloth.collideSphereOrigin[2]);
		glRotatef(
			qRadiansToDegrees(sphereRotationAngle),
			cloth.collideSphereAxisOfRotation[0],
			cloth.collideSphereAxisOfRotation[1],
			cloth.collideSphereAxisOfRotation[2]);
		// allow cloth to cover sphere, by making sphere slightly smaller
		glScalef(0.9f, 0.9f, 0.9f);
		gluSphere(sphereQuadratic, cloth.collideSphereRadius, 50, 50);
		glPopMatrix();
	}

	// cannot scale down if radius = 0
	if (mesh.boundingSphereRadius != 0.0f)
	{
		glPushMatrix();
		// move centre of mass to middle of world
		//glTranslatef(
		//	-mesh.centreOfMass.x(),
		//	-mesh.centreOfMass.y(),
		//	-mesh.centreOfMass.z());
		// scale mesh down to 1 unit
		//glScaled(
		//	1.0 / mesh.boundingSphereRadius,
		//	1.0 / mesh.boundingSphereRadius,
		//	1.0 / mesh.boundingSphereRadius);

		// draw it in grey
		glColor3f(0.7f, 0.7f, 0.7f);
		mesh.Draw();

		if (isVerticesDraggable)
		{
			// draw handles which allow you to drag around vertices
			glColor3f(0.4f, 0.4f, 0.1f);
			mesh.DrawVertexHandles(draggedVertexIndex);
		}

		glPopMatrix();
	}

	lastElapsedTime = elapsedTime;
}

QQuaternion OpenGLWidget::GetCameraQuaternion() const
{
	return
		QQuaternion::fromAxisAndAngle(QVector3D(1.0f, 0.0f, 0.0f), cameraRotation.x()) *
		QQuaternion::fromAxisAndAngle(QVector3D(0.0f, 1.0f, 0.0f), cameraRotation.y());
}

QString OpenGLWidget::GetRecordingFileName() const
{
	unsigned int recordingNumber = 1;
	QString currentFileName("recordings/recording 1.gif");

	// keep incrementing the recording number
	// until a fresh un-used filename is found
	while (QFile::exists(currentFileName))
		currentFileName = QString("recordings/recording ") % QString::number(++recordingNumber) % ".gif";

	return currentFileName;
}

size_t OpenGLWidget::GetSizeOfRecordingFrame()
{	
	// *4 for RGBA
	return (size_t)recordWidth * recordHeight * 4;
}

// mouse-handling
void OpenGLWidget::mousePressEvent(QMouseEvent* event)
{
	lastMouseX = event->x();
	lastMouseY = event->y();

	if (isVerticesDraggable)
	{
		// detect if there's a vertex under mouse on the first frame
		draggedVertexIndex = mesh.GetVertexIndexAtPixel(event->x(), height() - event->y());
		// is draggedVertexIndex inside fixedVertexIndices
		isDraggedVertexFixed = cloth.fixedVertexIndices.find(draggedVertexIndex) != cloth.fixedVertexIndices.end();
		if (isDraggedVertexFixed == false)
			// fix dragged vertex now
			cloth.fixedVertexIndices.insert(draggedVertexIndex);
	}
}

void OpenGLWidget::mouseMoveEvent(QMouseEvent* event)
{
	if (event->buttons() == Qt::MouseButton::NoButton)
	{
		if (isVerticesDraggable)
		{
			// this will highlight the hovered vertex, but will not drag yet1		
			draggedVertexIndex = mesh.GetVertexIndexAtPixel(event->x(), height() - event->y());
			// because it returns now
		}
		return;
	}

	// if mouse button is pressed, its dragging

	// calculate drag delta
	int dx = event->x() - lastMouseX;
	int dy = event->y() - lastMouseY;
	lastMouseX = event->x();
	lastMouseY = event->y();

	if (draggedVertexIndex == -1 || isVerticesDraggable == false)
	{
		// rotate camera with mouse drag

		float cameraRotationSpeed = 0.1f;
		// mouse x affects yaw
		// mouse y affects pitch
		cameraRotation.setX(qBound(-90.0f, cameraRotation.x() + cameraRotationSpeed * dy, 90.0f));
		cameraRotation.setY(cameraRotation.y() + cameraRotationSpeed * dx);
	}
	else
	{
		// move vertex with mouse drag

		float dragSpeed = 0.1f;
		// move vertex relative to camera's rotation
		QQuaternion inverseCameraQuaternion = GetCameraQuaternion().inverted();		
		mesh.vertices[draggedVertexIndex] +=
			inverseCameraQuaternion * QVector3D(
				dragSpeed * dx / scale,
				dragSpeed * -dy / scale, 0.0f);
	}
}

void OpenGLWidget::mouseReleaseEvent(QMouseEvent* event)
{
	if (isVerticesDraggable && 
		draggedVertexIndex != -1 && 
		isDraggedVertexFixed == false)
	{
		// need to remove the fixed vertex to return it to normal
		cloth.fixedVertexIndices.erase(draggedVertexIndex);
		// reset the vertex's velocity
		cloth.velocityField[draggedVertexIndex] = QVector3D();
	}
}

// key-handling
void OpenGLWidget::keyPressEvent(QKeyEvent* event)
{
	float moveSpeed = 0.001f / scale;	

	switch (event->key())
	{
	// Camera controls

	case Qt::Key::Key_W:
		cameraPosition += QVector3D(0.0f, 1.0f, 0.0f) * moveSpeed;
		break;

	case Qt::Key::Key_S:
		cameraPosition += QVector3D(0.0f, -1.0f, 0.0f) * moveSpeed;
		break;

	case Qt::Key::Key_A:
		cameraPosition += QVector3D(-1.0f, 0.0f, 0.0f) * moveSpeed;
		break;

	case Qt::Key::Key_D:
		cameraPosition += QVector3D(1.0f, 0.0f, 0.0f) * moveSpeed;
		break;

	case Qt::Key::Key_Escape:
		QApplication::quit();
		break;

	case Qt::Key::Key_L:
		LoadWavefrontModel();
		break;

	case Qt::Key::Key_O:
		SaveWavefrontModel();
		break;

	case Qt::Key::Key_T:
		LoadTextureImage();
		break;

	case Qt::Key::Key_R:
		ToggleRecord();
		break;

	case Qt::Key::Key_Return:
		StepSimulation();
		break;

	default:
		break;
	}
}

void OpenGLWidget::keyReleaseEvent(QKeyEvent* event)
{}

void OpenGLWidget::wheelEvent(QWheelEvent* event)
{
	float scroll = event->angleDelta().y() * 0.001f;
	scale *= 1.0f + scroll;
}

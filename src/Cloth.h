/*
===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
*/

#pragma once

#include <vector>
#include <set>

#include <QVector3D>

#include "Mesh.h"

class Cloth
{
public:

	class SpringConnection
	{
	public:
		unsigned int
			fromVertexIndex,
			toVertexIndex;
		float restLength;
		SpringConnection(
			unsigned int fromVertexIndex,
			unsigned int toVertexIndex,
			float restLength);
		// used for std::set
		bool operator<(const SpringConnection& other) const;
	};

	enum class CollideAgainst { none, plane, sphere };
	enum class Scenario
	{
		none,
		plane,
		sphere,
		flag,
		rotatingSphere,
		flagWithWind,
		fixedPerimeter
	};

	// initialise physics constants
	Cloth();

	// apply cloth physics to this target mesh
	Mesh* representingMesh;
	// set of unique pairs of vertices that represent springs
	std::vector<SpringConnection> springConnections;
	// a velocity value for every vertex with corresponding index
	std::vector<QVector3D> velocityField;
	// set of unique vertex indices that will not be have their positions updated
	std::set<unsigned int> fixedVertexIndices;

	// spring physics values:

	// spring constant k
	float springStiffness;
	// damper constant c
	float damperStiffness;
	// mass of whole cloth
	float mass;

	// current object for this cloth to collide against
	CollideAgainst collideAgainst;
	// coefficient of resitution, determines how bouncy the collider is
	// range [0,1]
	float colliderRestitution;

	// friction constant mu
	float frictionCoefficient;

	// collision values
	float collidePlaneOriginY;
	QVector3D collideSphereOrigin;
	float collideSphereRadius;
	float collideSphereAngularVelocity;
	QVector3D collideSphereAxisOfRotation;

	// wind
	QVector3D windPressureDirection;
	// seperated from direction for better GUI implementation
	float windPressureScalar;

	Scenario scenario;

	// change representation to another mesh
	void RepresentMesh(Mesh* mesh);
	// update the vertex positions of the mesh
	// with discrete time integration
	void Update(double deltaTime);
	// Fix vertices on the perimeter, assumes mesh is a grid
	void FixPerimeter();
	// Fix vertices on the corners of a mesh, assumes mesh is a grid
	void FixCorners(int numberOfCorners);
	// Setup a scenario with collideAgainst parameters and friction and air resistance if necessary
	void SetScenario(Scenario scenario);
	// Draw each vertex and spring with dots and lines
	void DebugDraw() const;

private:

	// calculates the mass of an individual vertex from total mass of cloth
	float VertexMass() const;

	// Applies friction to a vertex, given its in contact with a surface with a normal, and its force
	void ApplyFriction(
		QVector3D& velocityGlobal,
		const QVector3D& contactPointVelocity,		
		const QVector3D& collisionNormal,
		const QVector3D& force,
		double deltaTime);
	// Applies resitution (bouncy physics) on a collided point, with normal
	void ApplyRestitution(QVector3D& velocity, const QVector3D& normal);
	// Apply force to velocity but only deceleration
	void ApplyDeceleration(
		QVector3D& velocityGlobal,
		const QVector3D& velocityRelativeToContact,
		const QVector3D& force,
		double deltaTime);
	void ApplyWindForce(size_t vertexIndex, QVector3D& force);

	// Checks the vertex against current collider during this integration step.
	// Will update vertexPosition and velocity accordingly if collided and return true.
	// integratedDistance is the distance the vertex was moved by in the current intergration step
	bool CheckCollision(
		QVector3D& vertexPosition,
		QVector3D& velocity,
		float integratedDistance,
		QVector3D& outNormal,
		QVector3D& outContactPointVelocity);
	// Tests if a ray hits a sphere. If so, returns true and
	// outputs parameters where the ray hits the sphere
	// with enter/exit meaning relative to the ray direction
	bool RaycastAgainstSphere(
		QVector3D rayOrigin, QVector3D rayDirection,
		QVector3D sphereOrigin, float sphereRadius,
		float& outEnterRayParameter, float& outExitRayParameter);
	// get velocity at a point on sphere
	// if the sphere is rotating, this velocity changes
	QVector3D GetVelocityOnSphere(const QVector3D& point);
};

/*
===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
*/

// File adapted from Hamish Carr's AttributedObject.h

#pragma once

#include <vector>
#include <iostream>

#include "AdaptiveOpenGL.h"

#include <QVector3D>
#include <QVector2D>
#include <QString>

class Mesh
{
public:
    // raw unordered data
    std::vector<QVector3D> vertices;
    std::vector<QVector2D> textureCoordinates;
    std::vector<QVector3D> normals;

    // ordered data containing indices to which to read by
    std::vector<unsigned int> faceVertices;
    std::vector<unsigned int> faceTextureCoordinates;
    std::vector<unsigned int> faceNormals;

    // estimate of the centre of mass of the object
    QVector3D centreOfMass;
    // radius of an imaginary sphere (centred at centre of mass) that encapsulates this entire mesh
    float boundingSphereRadius;
    // number of sides on each face
    int faceSides;
    // opengl texture name
    GLuint textureName;
    // should we render with textures
    bool isTextureEnabled;

    // constructor will initialise to safe values
    Mesh();

    // clears all data stored in this mesh
    void Clear();

    // read and *add* data to this object from the stream in wavefront OBJ formatting
    // attempts to read as much as possible
    // i.e. continue reading even after formatting errors
    // returns false if nothing can be read
    bool ReadFromWavefront(std::istream& inputStream);

    // write contents of this object into the stream in wavefront OBJ formatting
    // returns true if the outputStream is good after the writing
    bool WriteToWavefront(std::ostream& outputStream) const;

    // Loads texture image into GPU, but doesn't enable texture rendering
    bool ReadFromTexture(const QString& textureFileName);

    // draw the mesh currently represented in this object in OpenGL
    void Draw() const;

    // draw dots that can be dragged by the user to move each vertex
    void DrawVertexHandles(int draggedVertexIndex) const;

    // gets the vertex index at a pixel coordinate on the screen
    // returns -1 if no vertex handle is at that coordinate
    int GetVertexIndexAtPixel(int x, int y) const;

    // move the centre of mass to a new position
    void MoveCentreOfMassTo(QVector3D position);

    // rewrite all normals with approximates using cross product
    void RecalculateNormals(bool areNormalsInverted);
};

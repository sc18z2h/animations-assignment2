/*
===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
*/

#include <QtWidgets>
#include <QSizePolicy>

#include "OpenGLWidget.h"

constexpr float MAX_SPRING_STIFFNESS = 10.0f;
constexpr float MAX_DAMPER_STIFFNESS = 10.0f;
constexpr float MAX_CLOTH_MASS = 10.0f;
constexpr float MAX_WIND_PRESSURE = 10.0f;

int main(int argc, char** argv)
{
	QApplication application(argc, argv);
    
    // represents the entire window
    QWidget window;
    // portion responsible for OpenGL
    OpenGLWidget openGLWidget(nullptr);
    // portion responsible for Qt UI controls
    QWidget controlPanel;

    QHBoxLayout horizontalLayout;
    QFormLayout formLayout;
    
    QPushButton loadModelButton("Load Wavefront .obj [L]");
    QPushButton saveModelButton("Save Wavefront .obj [O]");    
    QLabel meshInfoLabel;

    QPushButton loadTextureButton("Load Texture Image [T]");
    QCheckBox isTextureEnabledCheckbox;

    QCheckBox areVerticesDraggableCheckbox;
    QPushButton playPauseButton("Play");
    QPushButton stepSimulationButton("Simulate 1 Step");

    QLabel springStiffnessLabel; QSlider springStiffnessSlider(Qt::Orientation::Horizontal);
    QLabel damperStiffnessLabel; QSlider damperStiffnessSlider(Qt::Orientation::Horizontal);
    QLabel massLabel; QSlider massSlider(Qt::Orientation::Horizontal);

    QLabel restitutionLabel; QSlider restitutionSlider(Qt::Orientation::Horizontal);
    QLabel frictionLabel; QSlider frictionSlider(Qt::Orientation::Horizontal);
    QLabel windPressureLabel; QSlider windPressureSlider(Qt::Orientation::Horizontal);

    QComboBox scenarioSelector;

    QPushButton startStopRecordingButton("Start recording .gif [R]");

    // ints represent percentage, [  1%, 100%]
    springStiffnessSlider.setMinimum(1);
    springStiffnessSlider.setMaximum(100);
    damperStiffnessSlider.setMinimum(1);
    damperStiffnessSlider.setMaximum(100);
    // cannot be too light, otherwise physics diverges
    massSlider.setMinimum(20);
    massSlider.setMaximum(100);
    restitutionSlider.setMinimum(0);
    restitutionSlider.setMaximum(100);
    frictionSlider.setMinimum(0);
    frictionSlider.setMaximum(100);
    windPressureSlider.setMinimum(0);
    windPressureSlider.setMaximum(100);

    // connect loading buttons
    QObject::connect(&loadModelButton, &QPushButton::pressed,
        [&openGLWidget, &meshInfoLabel]()
        {
            openGLWidget.LoadWavefrontModel();
        });
    QObject::connect(&openGLWidget, &OpenGLWidget::OnMeshChanged,
        [&meshInfoLabel](Mesh& newMesh)
        {
            if (0 != newMesh.faceSides)
                meshInfoLabel.setText
                (
                    "vertices: " % QString::number(newMesh.vertices.size()) % '\n' %
                    "faces: " % QString::number(newMesh.faceVertices.size() / newMesh.faceSides) % '\n' %
                    "number of sides: " % QString::number(newMesh.faceSides)
                );
            else
                meshInfoLabel.clear();
        });
    QObject::connect(&saveModelButton, &QPushButton::pressed,
        [&openGLWidget]()
        {
            openGLWidget.SaveWavefrontModel();
        });
    QObject::connect(&loadTextureButton, &QPushButton::pressed,
        [&openGLWidget, &isTextureEnabledCheckbox]()
        {
            openGLWidget.LoadTextureImage();
        });

    // connect texture enabling control
    QObject::connect(&isTextureEnabledCheckbox, &QCheckBox::stateChanged,
        [&openGLWidget](int state)
        {
            openGLWidget.mesh.isTextureEnabled = state != 0;
        });
    QObject::connect(&openGLWidget, &OpenGLWidget::OnIsTextureEnabledChanged,
        [&isTextureEnabledCheckbox](bool isTextureEnabled)
        {
            isTextureEnabledCheckbox.setChecked(isTextureEnabled);
        });

    // connect simulation controls
    QObject::connect(&areVerticesDraggableCheckbox, &QCheckBox::stateChanged,
        [&openGLWidget](int state)
        {
            openGLWidget.isVerticesDraggable = state != 0;
        });

    QObject::connect(&playPauseButton, &QPushButton::pressed,
        [&openGLWidget, &playPauseButton]()
        {
            openGLWidget.isSimulationPlaying = !openGLWidget.isSimulationPlaying;
            playPauseButton.setText(openGLWidget.isSimulationPlaying ? "Pause" : "Play");
        });
    QObject::connect(&stepSimulationButton, &QPushButton::pressed,
        [&openGLWidget, &playPauseButton]()
        {
            openGLWidget.StepSimulation();
            playPauseButton.setText("Play");
        });

    // connect slider variables
    QObject::connect(&springStiffnessSlider, &QSlider::valueChanged,
        [&openGLWidget, &springStiffnessLabel](int value)
        {
            openGLWidget.cloth.springStiffness = (value / 100.0f) * MAX_SPRING_STIFFNESS;
            springStiffnessLabel.setText("Spring Stiffness k (" +
                QString::number(openGLWidget.cloth.springStiffness) + ")");
        });
    QObject::connect(&damperStiffnessSlider, &QSlider::valueChanged,
        [&openGLWidget, &damperStiffnessLabel](int value)
        {
            openGLWidget.cloth.damperStiffness = (value / 100.0f) * MAX_DAMPER_STIFFNESS;
            damperStiffnessLabel.setText("Damper Stiffness c (" +
                QString::number(openGLWidget.cloth.damperStiffness) + ")");
        });
    QObject::connect(&massSlider, &QSlider::valueChanged,
        [&openGLWidget, &massLabel](int value)
        {
            openGLWidget.cloth.mass = (value / 100.0f) * MAX_CLOTH_MASS;
            massLabel.setText("Cloth total mass kg (" +
                QString::number(openGLWidget.cloth.mass) + ")");
        });

    QObject::connect(&restitutionSlider, &QSlider::valueChanged,
        [&openGLWidget, &restitutionLabel](int value)
        {
            openGLWidget.cloth.colliderRestitution = value / 100.0f; // range [0, 1]
            restitutionLabel.setText("Restitution bounciness (" +
                QString::number(openGLWidget.cloth.colliderRestitution) + ")");
        });
    QObject::connect(&frictionSlider, &QSlider::valueChanged,
        [&openGLWidget, &frictionLabel](int value)
        {
            openGLWidget.cloth.frictionCoefficient = value / 100.0f; // range [0, 1]
            frictionLabel.setText("Friction coefficient (" +
                QString::number(openGLWidget.cloth.frictionCoefficient) + ")");
        });
    QObject::connect(&windPressureSlider, &QSlider::valueChanged,
        [&openGLWidget, &windPressureLabel](int value)
        {
            openGLWidget.cloth.windPressureScalar = (value / 100.0f) * MAX_WIND_PRESSURE;
            windPressureLabel.setText("Wind pressure (" +
                QString::number(openGLWidget.cloth.windPressureScalar) + ")");
        });

    // connect scenario selector
    void (QComboBox::* signal)(int) = &QComboBox::currentIndexChanged;
    QObject::connect(&scenarioSelector, signal,
        [&openGLWidget](int index)
        {
            openGLWidget.cloth.SetScenario((Cloth::Scenario)index);
        });

    // record button
    QObject::connect(&startStopRecordingButton, &QPushButton::pressed,
        [&openGLWidget, &startStopRecordingButton]()
        {
            openGLWidget.ToggleRecord();
            startStopRecordingButton.setText(openGLWidget.isRecording ?
                "Stop recording .gif [R]" : "Start recording .gif [R]");
        });

    // set values, to update labels and sliders to default values
    // convert to percentages
    springStiffnessSlider.setValue(100 * openGLWidget.cloth.springStiffness / MAX_SPRING_STIFFNESS);
    damperStiffnessSlider.setValue(100 * openGLWidget.cloth.damperStiffness / MAX_DAMPER_STIFFNESS);
    massSlider.setValue(100 * openGLWidget.cloth.mass / MAX_CLOTH_MASS);
    restitutionSlider.setValue(100 * openGLWidget.cloth.colliderRestitution);
    frictionSlider.setValue(100 * openGLWidget.cloth.frictionCoefficient);
    windPressureSlider.setValue(100 * openGLWidget.cloth.windPressureScalar / MAX_WIND_PRESSURE);
    // force events to fire off, sometimes they don't
    springStiffnessSlider.valueChanged(springStiffnessSlider.value());
    damperStiffnessSlider.valueChanged(damperStiffnessSlider.value());
    massSlider.valueChanged(massSlider.value());
    restitutionSlider.valueChanged(restitutionSlider.value());
    frictionSlider.valueChanged(frictionSlider.value());
    windPressureSlider.valueChanged(windPressureSlider.value());

    // Create scenario options
    /*  
    enum class Scenario
	{
		none,
		plane,
		sphere,
		flag,
		rotatingSphere,
		flagWithWind,
		fixedPerimeter
	};
    */
    scenarioSelector.addItems
    ({
        "Empty",
        "3. Plane",
        "4. (SS1) Sphere",
        "5. (SS2) Two corners",
        "6. (SS1+) Rotating sphere",
        "7. (SS2+) Two corners with wind",
        "Net"
    });

    window.setLayout(&horizontalLayout);
    horizontalLayout.addWidget(&openGLWidget, 1);
    horizontalLayout.addWidget(&controlPanel);

    controlPanel.setLayout(&formLayout);

    formLayout.addWidget(&loadModelButton);
    formLayout.addWidget(&saveModelButton);

    formLayout.addWidget(&loadTextureButton);
    formLayout.addRow("Is Texture Enabled?", &isTextureEnabledCheckbox);

    formLayout.addWidget(&meshInfoLabel);
    formLayout.addRow("Are Vertices Draggable?", &areVerticesDraggableCheckbox);

    formLayout.addWidget(&playPauseButton);
    formLayout.addWidget(&stepSimulationButton);

    formLayout.addRow(&springStiffnessLabel, &springStiffnessSlider);
    formLayout.addRow(&damperStiffnessLabel, &damperStiffnessSlider);
    formLayout.addRow(&massLabel, &massSlider);
    
    formLayout.addRow(&restitutionLabel, &restitutionSlider);
    formLayout.addRow(&frictionLabel, &frictionSlider);
    formLayout.addRow(&windPressureLabel, &windPressureSlider);

    formLayout.addRow("Selected Scenario", &scenarioSelector);

    formLayout.addWidget(&startStopRecordingButton);

    window.setWindowTitle("IK OpenGL Display");
    window.setMinimumSize(1280, 720);
    // move to middle of screen
    window.move(QApplication::desktop()->screen()->rect().center() - window.rect().center());
    window.show();

    // prevent side panel from resizing due to label growing or shrink in size
    restitutionLabel.setMinimumWidth(restitutionLabel.width() + 20);

	return application.exec();
}

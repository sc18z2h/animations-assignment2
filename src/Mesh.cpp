/*
===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
*/

#include <sstream>
#include <string>
#include <cctype>
#include <limits>

#include <QDebug>
#include <QImage>

#include "Mesh.h"
#include "AdaptiveOpenGL.h"

using namespace std;

Mesh::Mesh() :
    boundingSphereRadius(0.0f),
    faceSides(0),
    isTextureEnabled(false),
    textureName(0)
{}

void Mesh::Clear()
{
    vertices.clear();
    textureCoordinates.clear();
    normals.clear();

    faceVertices.clear();
    faceTextureCoordinates.clear();
    faceNormals.clear();

    centreOfMass = QVector3D();
    boundingSphereRadius = 0.0f;
    faceSides = 0;
}

bool Mesh::ReadFromWavefront(std::istream& inputStream)
{
    string line;
    bool readData = false;
    // used to delimit whitespaces inside of lines
    istringstream inputStringStream;
    // used later to know whether any useful data was read from stream
    size_t beforeDataTotalSize =
        vertices.size() +
        textureCoordinates.size() +
        normals.size() +

        faceVertices.size() +
        faceTextureCoordinates.size() +
        faceNormals.size();

    // iterate through lines
    while (getline(inputStream, line))
    {
        inputStringStream.str(line);

        // used to temporarily hold data being read
        QVector3D readVector3D;
        QVector2D readVector2D;

        // inspect 1st character
        switch (line[0])
        {
        case '#': // comment
            // discard
            break;

        case 'v': // vertex data of some type
            // inspect 2nd character
            switch (line[1])
            {
            case ' ': // geometric position                
                // skip the "v "
                inputStringStream.seekg(2);
                inputStringStream >>
                    readVector3D[0] >> readVector3D[1] >> readVector3D[2];
                vertices.push_back(readVector3D);
                break;
            case 't': // texture coordinate
                // skip the "vt "
                inputStringStream.seekg(3);
                inputStringStream >>
                    readVector2D[0] >> readVector2D[1];
                textureCoordinates.push_back(readVector2D);
                break;
            case 'n': // normal
                // skip the "vn "
                inputStringStream.seekg(3);
                inputStringStream >>
                    readVector3D[0] >> readVector3D[1] >> readVector3D[2];
                normals.push_back(readVector3D);
                break;
            default: // format error
                break;
            }
            break;

        case 'f': // face index data
            if (faceSides == 0)
            {
                // find faceSides

                // is the previous character a whitespace character?
                // start with true, to be able to count the first declaration
                bool isPreviousWhiteSpace = true;
                // count the number of vertex declarations
                // vertex declarations contain continuous non-whitespace characters '/' and ints
                size_t declarationsCount = 0;
                for (size_t i = 2; i < line.size(); ++i)
                {
                    bool isWhiteSpace = isspace(line[i]);
                    if (isPreviousWhiteSpace == true && isWhiteSpace == false)
                        // moved into a vertex declaration
                        ++declarationsCount;
                    isPreviousWhiteSpace = isWhiteSpace;
                }

                // set the number of sides on each face
                faceSides = declarationsCount;
            }

            // faceSides is now known
            // assume its the same for all declarations
            // skip the "f "
            inputStringStream.seekg(2);

            // iterate through everyside            
            for (int i = 0; i < faceSides; ++i)
            {
                unsigned int vertexIndex = 1, textureIndex = 1, normalIndex = 1;
                char bracket; // dummy for extracting brackets                
                inputStringStream >>
                    vertexIndex >> bracket >>
                    textureIndex >> bracket >>
                    normalIndex;
                // our indices are 0-based, but wavefront is 1-based
                faceVertices.push_back(vertexIndex - 1);
                faceTextureCoordinates.push_back(textureIndex - 1);
                faceNormals.push_back(normalIndex - 1);
            }
            break;

        default:
            // also, discard
            break;
        }                
    }

    if (0 != vertices.size())
    {
        // recalculate centre of mass
        // initialise to 0 vector
        centreOfMass = QVector3D();
        for (size_t i = 0; i < vertices.size(); ++i)
            centreOfMass += vertices[i];
        centreOfMass /= vertices.size();

        // recalculate boundingSphereRadius
        boundingSphereRadius = 0.0f;
        for (size_t i = 0; i < vertices.size(); ++i)
        {
            float distanceFromCentre = (vertices[i] - centreOfMass).length();
            if (0 < distanceFromCentre)
                boundingSphereRadius = distanceFromCentre;
        }
    }

    size_t afterDataTotalSize =
        vertices.size() +
        textureCoordinates.size() +
        normals.size() +

        faceVertices.size() +
        faceTextureCoordinates.size() +
        faceNormals.size();


    if (255 < vertices.size())
        qWarning() << "More than 255 vertices. Drawing vertex handles might have the wrong values due to finite precision.";

    // return true if we've read any data from the stream
    return beforeDataTotalSize < afterDataTotalSize;
}

bool Mesh::WriteToWavefront(std::ostream& outputStream) const
{
    //outputStream << signature << endl;

    // write vertices v statements
    for (size_t i = 0; i < vertices.size(); ++i)
        outputStream << "v " << 
        vertices[i][0] << ' ' <<
        vertices[i][1] << ' ' <<
        vertices[i][2] << endl;
    // write texture coordinate vt statements
    for (size_t i = 0; i < textureCoordinates.size(); ++i)
        outputStream << "vt " <<
        textureCoordinates[i][0] << ' ' <<
        textureCoordinates[i][1] << endl;
    // write normal vn statements
    for (size_t i = 0; i < normals.size(); ++i)
        outputStream << "vn " <<
        normals[i][0] << ' ' <<
        normals[i][1] << ' ' <<
        normals[i][2] << endl;

    // write face f statements
    for (size_t faceStartIndex = 0;
        faceStartIndex < faceVertices.size();
        faceStartIndex += faceSides)
    {
        outputStream << 'f';
        for (size_t vertexOffset = 0;
            vertexOffset < faceSides;
            ++vertexOffset)
        {
            // prepend space
            outputStream << ' ' <<
                // our indices are 0-based, but wavefront is 1-based
                faceVertices[faceStartIndex + vertexOffset] + 1 << '/' <<
                faceTextureCoordinates[faceStartIndex + vertexOffset] + 1 << '/' <<
                faceNormals[faceStartIndex + vertexOffset] + 1;
        }
        outputStream << endl;
    }

    // return if good as per spec
    return outputStream.good();
}

bool Mesh::ReadFromTexture(const QString& textureFileName)
{
    QImage rgbaImage = QImage(textureFileName)
        // good format for opengl
        .convertToFormat(QImage::Format::Format_RGB888);
    if (rgbaImage.isNull())
        // cannot open texture file
        return false;

    // reset texture by deleting it and generating it again
    glDeleteTextures(1, &textureName);
    glGenTextures(1, &textureName);
    // now bind to it - i.e. all following code addresses this one
    glBindTexture(GL_TEXTURE_2D, textureName);
    // set these parameters to avoid dealing with mipmaps 
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    // now transfer the image
    glTexImage2D
    (
        GL_TEXTURE_2D,      // it's a 2D texture
        0,                  // mipmap level of 0 (ie the largest one)
        GL_RGBA,            // we want the data stored as RGBA on GPU
        rgbaImage.width(),  // width of the image
        rgbaImage.height(), // height of the image
        0,                  // width of border (in texels)
        GL_RGB,             // format the data is stored in on CPU
        GL_UNSIGNED_BYTE,   // data type
        rgbaImage.constBits()// and a pointer to the data
    );
    
    return true;
}

void Mesh::Draw() const
{
    if (isTextureEnabled)
    {
        glEnable(GL_TEXTURE_2D);
        // use our texture
        glBindTexture(GL_TEXTURE_2D, textureName);
    }
    else
        // ensure its disabled
        glDisable(GL_TEXTURE_2D);

    if (faceSides == 3)
    {
        glBegin(GL_TRIANGLES);

        for (size_t faceStartIndex = 0;
            faceStartIndex < faceVertices.size();
            faceStartIndex += 3)
        {
            for (size_t vertexOffset = 0;
                vertexOffset < 3;
                ++vertexOffset)
            {
                const QVector3D& vertex = vertices[
                    faceVertices[faceStartIndex + vertexOffset]];
                const QVector2D& textureCoordinate = textureCoordinates[
                    faceTextureCoordinates[faceStartIndex + vertexOffset]];
                const QVector3D& normal = normals[
                    faceNormals[faceStartIndex + vertexOffset]];
                
                glTexCoord2f(textureCoordinate[0], textureCoordinate[1]);
                glNormal3f(normal[0], normal[1], normal[2]);
                // must set vertex last
                glVertex3f(vertex[0], vertex[1], vertex[2]);
            }
        }

        glEnd();
    }
    else
    {
        for (size_t faceStartIndex = 0;
            faceStartIndex < faceVertices.size();
            faceStartIndex += faceSides)
        {
            glBegin(GL_POLYGON);
            for (size_t vertexOffset = 0;
                vertexOffset < faceSides;
                ++vertexOffset)
            {
                const QVector3D& vertex = vertices[
                    faceVertices[faceStartIndex + vertexOffset]];
                const QVector2D& textureCoordinate = textureCoordinates[
                    faceTextureCoordinates[faceStartIndex + vertexOffset]];
                const QVector3D& normal = normals[
                    faceNormals[faceStartIndex + vertexOffset]];
                glTexCoord2f(textureCoordinate[0], textureCoordinate[1]);
                glNormal3f(normal[0], normal[1], normal[2]);
                // must set vertex last
                glVertex3f(vertex[0], vertex[1], vertex[2]);
            }
            glEnd();
        }
    }

    if (isTextureEnabled)
        // reset texture state, so other objects can draw without problems
        glDisable(GL_TEXTURE_2D);
}

const int USE_CHANNEL = 3;

void Mesh::DrawVertexHandles(int draggedVertexIndex) const
{
    // draw control points
    // store the states before
    GLboolean
        wasDepthTestEnabled = true,
        wasLightingEnabled = true;
    glGetBooleanv(GL_DEPTH_TEST, &wasDepthTestEnabled);
    glGetBooleanv(GL_LIGHTING, &wasLightingEnabled);
    
    // the handles must be unlit and appear "on top" of everything else
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);

    // get the drawing vertex color in ints
    GLfloat currentColor[4] = { 0 };
    glGetFloatv(GL_CURRENT_COLOR, currentColor);
    // save original alpha
    GLfloat beforeColorAlpha = currentColor[USE_CHANNEL];
    // begin drawing each vertex as a point
    glPointSize(8.0f);

    glBegin(GL_POINTS);    
    for (size_t i = 0; i < vertices.size(); ++i)
    {
        // encode vertex index into alpha value
        //                             + 0.5f
        // because when we convert back from float into int, it'll be more reliable
        currentColor[USE_CHANNEL] = (i + 0.5f) / (float) vertices.size();

        // if this vertex is being dragged, give it a brighter colour
        if (i == draggedVertexIndex)
            glColor4f(
                currentColor[0] * 2.5f,
                currentColor[1] * 2.5f,
                currentColor[2] * 2.5f,
                currentColor[3]);
        else
            glColor4fv(currentColor);

        glVertex3f(vertices[i][0], vertices[i][1], vertices[i][2]);
    }
    glEnd();
    // reset alpha value
    currentColor[USE_CHANNEL] = beforeColorAlpha;
    glColor4fv(currentColor);
    
    // restore states before
    if (wasDepthTestEnabled)
        glEnable(GL_DEPTH_TEST);
    if (wasLightingEnabled)
        glEnable(GL_LIGHTING);
}

int Mesh::GetVertexIndexAtPixel(int x, int y) const
{
    GLfloat colorAtPixel[4] = { 0 };
    glReadPixels(x, y, 1, 1, GL_RGBA, GL_FLOAT, colorAtPixel);
    
    // index is encoded in the alpha channel
    int vertexIndex = (int)(colorAtPixel[USE_CHANNEL] * vertices.size());

    if (0 <= vertexIndex && vertexIndex < vertices.size())
        return vertexIndex;
    // return -1 as per spec
    else
        return -1;
}

void Mesh::MoveCentreOfMassTo(QVector3D position)
{
    // vector to add to all vertices to apply the change
    QVector3D updateVector = position - centreOfMass;
    for (size_t i = 0; i < vertices.size(); ++i)
        vertices[i] += updateVector;
    centreOfMass = position;
}

void Mesh::RecalculateNormals(bool areNormalsInverted)
{
    if (faceSides < 3)
        // cannot use triangle crossproduct approximation
        return;

    // recreate normals, 1 normal for every face
    normals.clear();
    normals.resize(faceNormals.size() / faceSides);

    int normalInvertFactor = areNormalsInverted ? -1 : +1;

    for (size_t faceStartIndex = 0;
        faceStartIndex < faceNormals.size();
        faceStartIndex += faceSides)
    {
        // approximate the whole face using the first 3 vertices as triangle
        QVector3D& vertex0 = vertices[faceVertices[faceStartIndex + 0]];
        QVector3D& vertex1 = vertices[faceVertices[faceStartIndex + 1]];
        QVector3D& vertex2 = vertices[faceVertices[faceStartIndex + 2]];

        // this index gets the face index
        normals[faceStartIndex / faceSides] = QVector3D::crossProduct
        (
            vertex1 - vertex0,
            vertex2 - vertex0
        ).normalized() * normalInvertFactor;

        // set all normal indices of faceNormals on this face
        for (size_t vertexOffset = 0;
            vertexOffset < faceSides;
            ++vertexOffset)
            faceNormals[faceStartIndex + vertexOffset] = faceStartIndex / faceSides;
    }
}

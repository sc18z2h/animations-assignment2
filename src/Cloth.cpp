/*
===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
*/

#include "Cloth.h"

using namespace std;

// Constants for collision detection
constexpr QVector3D COLLIDE_PLANE_NORMAL(0.0f, 1.0f, 0.0f);

Cloth::SpringConnection::SpringConnection(
	unsigned int fromVertexIndex,
	unsigned int toVertexIndex,
	float restLength) :
	// set to min, so pairing is unique
	fromVertexIndex(min(fromVertexIndex, toVertexIndex)),
	// set to max, so pairing unique
	toVertexIndex(max(fromVertexIndex, toVertexIndex)),
	restLength(restLength)	
{}

bool Cloth::SpringConnection::operator<(const SpringConnection& other) const
{
	// fromVertexIndex major, toVertexIndex minor
	if (fromVertexIndex == other.fromVertexIndex)
		return toVertexIndex < other.toVertexIndex;
	else
		return fromVertexIndex < other.fromVertexIndex;
}

Cloth::Cloth() :
	representingMesh(nullptr),
	springStiffness(5.0f),
	damperStiffness(0.1f),
	mass(0.5f),

	collideAgainst(Cloth::CollideAgainst::sphere),
	colliderRestitution(0.5f),
	frictionCoefficient(1.0f),

	collidePlaneOriginY(0.0f),

	collideSphereOrigin(0.0f, 1.0f, 0.0f),
	collideSphereRadius(1.0f),
	collideSphereAngularVelocity(10.0f),
	collideSphereAxisOfRotation(0.0f, 1.0f, 0.0f),

	scenario(Scenario::none),
	windPressureScalar(1.0f)
{}

void Cloth::RepresentMesh(Mesh* mesh)
{
	representingMesh = mesh;

	// reset current data
	springConnections.clear();
	velocityField.clear();

	if (representingMesh == nullptr)
		// cannot be missing a representation
		return;

	// velocity value for each vertex is needed
	// initial value of 0
	velocityField.resize(mesh->vertices.size());

	// set of currently added connections that are unique
	set<Cloth::SpringConnection> uniqueConnections;

	// initialise springConnections
	// iterate through every face
	for (size_t faceStartIndex = 0;
		faceStartIndex < mesh->faceVertices.size();
		faceStartIndex += mesh->faceSides)
	{
		// create spring connection from
		// every vertex of this face to
		// every other vertex
		// but we must ensure the connections are unique
		for (size_t fromVertexOffset = 0;
			fromVertexOffset < mesh->faceSides;
			++fromVertexOffset)
		{
			// nested loop through unique vertex pairs to ensure uniqueness within this face
			for (size_t toVertexOffset = fromVertexOffset + 1;
				toVertexOffset < mesh->faceSides;
				++toVertexOffset)
			{
				size_t
					fromVertexIndex = mesh->
						faceVertices[faceStartIndex + fromVertexOffset],
					toVertexIndex = mesh->
						faceVertices[faceStartIndex + toVertexOffset];

				float restLength =
					(
						mesh->vertices[fromVertexIndex] -
						mesh->vertices[toVertexIndex]
					).length();

				Cloth::SpringConnection springConnection(
					fromVertexIndex,
					toVertexIndex,
					restLength);

				// check uniqueness compared to other faces
				// if springConnection is not in uniqueConnections
				if (uniqueConnections.find(springConnection) == uniqueConnections.end())
				{
					uniqueConnections.insert(springConnection);
					springConnections.push_back(springConnection);
				}
			}
		}
	}

	// set up the same scenario again
	SetScenario(scenario);
}

void Cloth::Update(double deltaTime)
{
	if (representingMesh == nullptr)
		// cannot be missing a representation
		return;

	// deltaTime cannot be larger than 0.02,
	// otherwise it will diverge too much from a reasonable simulation
	deltaTime = min(0.02, deltaTime);

	// a total summed force value for every vertex with corresponding index
	// these can accelerate and deccelerate
	// initialised to 0
	vector<QVector3D> forceField(velocityField.size());
	// a seperate force field for damper forces,
	// these must only deccelerate the vertices, never accelerate them
	// initialised to 0
	vector<QVector3D> damperField(velocityField.size());

	// loop through springs
	for (size_t i = 0; i < springConnections.size(); ++i)
	{
		unsigned int
			fromIndex = springConnections[i].fromVertexIndex,
			toIndex = springConnections[i].toVertexIndex;

		// vector that moves from position to position, will be normalised
		QVector3D fromToVector =
			representingMesh->vertices[fromIndex] -
			representingMesh->vertices[toIndex];
		// current length of spring
		float currentLength = fromToVector.length();

		// edge case, really unfortunate if this happens
		if (currentLength == 0.0f)
			continue;

		// normalise fromToVector
		fromToVector /= currentLength;

	 	QVector3D fromVelocity = velocityField[fromIndex];
		velocityField[toIndex];

		// the scalar of spring force along the fromToVector, can accelerate and deccelerate
		float springForce = -springStiffness *
		(
			currentLength - springConnections[i].restLength
		);
		// the scalar of damper along the fromToVector, can only deccelerate
		float damperForce = damperStiffness * QVector3D::dotProduct
		(
			velocityField[toIndex] - velocityField[fromIndex],
			fromToVector
		);

		// add forces to running totals
		forceField[fromIndex] += springForce * fromToVector;
		damperField[fromIndex] += damperForce * fromToVector;
		// remember equal and opposite forces
		forceField[toIndex] -= springForce * fromToVector;
		damperField[toIndex] -= damperForce * fromToVector;
	}

	// now that forces are all summed up
	// apply acceleration from forces
	for (size_t i = 0; i < forceField.size(); ++i)
	{
		// if set contains i
		if (fixedVertexIndices.find(i) != fixedVertexIndices.end())
			// don't update velocity
			continue;
		
		// add gravity force, gravity might affect friction later on
		forceField[i] += VertexMass() * QVector3D(0.0f, -9.81f, 0.0f);

		// add wind force
		ApplyWindForce(i, forceField[i]);

		// apply acceleration/decceleration forces
		velocityField[i] += forceField[i] / VertexMass() * deltaTime;

		// apply decceleration only forces
		ApplyDeceleration(
			velocityField[i],
			velocityField[i],
			damperField[i],
			deltaTime);
	}

	// apply velocity step
	for (size_t i = 0; i < velocityField.size(); ++i)
	{
		// if set contains i
		if (fixedVertexIndices.find(i) != fixedVertexIndices.end())
			// don't update position
			continue;

		// update position with velocity
		QVector3D move = velocityField[i] * deltaTime;
		representingMesh->vertices[i] += move;

		// after position is updated, check for collisions
		QVector3D collisionNormal;
		QVector3D contactPointVelocity;
		if (CheckCollision(
			representingMesh->vertices[i],
			velocityField[i],
			move.length(),
			collisionNormal,
			contactPointVelocity))
		{
			ApplyFriction(
				velocityField[i],
				contactPointVelocity,
				collisionNormal,
				forceField[i],
				deltaTime);

			// finally, after friction and collision
			// apply restitution
			ApplyRestitution(velocityField[i], collisionNormal);
		}
	}

	// normals are changed
	representingMesh->RecalculateNormals(false);
}

void Cloth::FixPerimeter()
{
	// reset fixed vertices
	fixedVertexIndices.clear();

	if (representingMesh == nullptr)
		return;

	// number of connected faces for each vertex, corresponding to index
	vector<unsigned int> connectedFaces(representingMesh->vertices.size());

	for (size_t i = 0; i < representingMesh->faceVertices.size(); ++i)
		++connectedFaces[representingMesh->faceVertices[i]];

	for (size_t i = 0; i < connectedFaces.size(); ++i)
		if (connectedFaces[i] != representingMesh->faceSides)
			// vertex i is on the perimeter
			fixedVertexIndices.insert(i);
}

void Cloth::FixCorners(int numberOfCorners)
{
	// reset fixed vertices
	fixedVertexIndices.clear();

	if (representingMesh == nullptr)
		return;

	// number of connected faces for each vertex, corresponding to index
	vector<unsigned int> connectedFaces(representingMesh->vertices.size());

	for (size_t i = 0; i < representingMesh->faceVertices.size(); ++i)
		++connectedFaces[representingMesh->faceVertices[i]];

	// on a grid, a corner vertex has 2 less connected edges
	int cornerVertexDegree = representingMesh->faceSides - 2;
	for (size_t i = 0; i < connectedFaces.size(); ++i)
		if (connectedFaces[i] == 1)
		{
			// vertex i is on the corner
			fixedVertexIndices.insert(i);
			// count down from numberOfCorners until it reaches 0
			if (--numberOfCorners <= 0)
				return;
		}
}

void Cloth::SetScenario(Scenario scenario)
{
	this->scenario = scenario;

	// first reset shared parameters
	fixedVertexIndices.clear();
	collideAgainst = CollideAgainst::none;
	collideSphereAngularVelocity = 0.0f;
	windPressureDirection = QVector3D();

	switch (scenario)
	{
	case Scenario::plane:
		collideAgainst = CollideAgainst::plane;
		break;

	case Scenario::sphere:
		collideAgainst = CollideAgainst::sphere;
		break;

	case Scenario::flag:
		FixCorners(2);
		break;

	case Scenario::rotatingSphere:
		collideAgainst = CollideAgainst::sphere;
		collideSphereAngularVelocity = 10.0f;
		break;

	case Scenario::flagWithWind:
		FixCorners(2);
		windPressureDirection = QVector3D(0.1f, 0.0f, -1.0f).normalized();
		break;

	case Scenario::fixedPerimeter:
		FixPerimeter();
		break;

	default:
		break;
	}
}

#include "AdaptiveOpenGL.h"

void Cloth::DebugDraw() const
{
	if (representingMesh == nullptr)
		// cannot be missing a representation
		return;

	glLineWidth(3.0f);
	glPointSize(5.0f);

	glBegin(GL_POINTS);
	for (size_t i = 0; i < representingMesh->vertices.size(); ++i)
	{
		glVertex3f(representingMesh->vertices[i][0],
			representingMesh->vertices[i][1],
			representingMesh->vertices[i][2]);
	}
	glEnd();

	for (size_t i = 0; i < springConnections.size(); ++i)
	{
		const QVector3D&
			fromVertex = representingMesh->vertices[springConnections[i].fromVertexIndex],
			toVertex = representingMesh->vertices[springConnections[i].toVertexIndex];

		glColor3f(0.3f, 0.3f, 0.3f);
		glBegin(GL_LINES);
		glVertex3f(fromVertex[0], fromVertex[1], fromVertex[2]);
		glVertex3f(toVertex[0], toVertex[1], toVertex[2]);
		glEnd();

		glColor3f(1.0f, 1.0f, 1.0f);
		glBegin(GL_POINTS);
		glVertex3f(fromVertex[0], fromVertex[1], fromVertex[2]);
		glVertex3f(toVertex[0], toVertex[1], toVertex[2]);
		glEnd();
	}	
}

float Cloth::VertexMass() const
{
	// velocityField.size() is the same as vertices.size()
	return mass / velocityField.size();
}

void Cloth::ApplyFriction(
	QVector3D& velocityGlobal,
	const QVector3D& contactPointVelocity,
	const QVector3D& collisionNormal,
	const QVector3D& force,
	double deltaTime)
{
	// now apply friction
	// friction direction changes depending on the contact's velocity
	QVector3D velocityRelativeToContact = velocityGlobal - contactPointVelocity;
	// cross product to find the orthogonal friction direction
	QVector3D biTangent = QVector3D::crossProduct
	(
		velocityRelativeToContact,
		collisionNormal
	);
	// check velocity and normal are not parallel
	if (0 < biTangent.lengthSquared())
	{
		// cross product again to get the direction opposite to velocity
		// but parallel to surface / orthogonal to normal
		QVector3D frictionDirection = QVector3D::crossProduct
		(
			biTangent,
			collisionNormal
		).normalized();
		// force along normal direction
		float normalForce = QVector3D::dotProduct
		(
			force,
			-collisionNormal // we want normal force to be positive
		);

		ApplyDeceleration(
			velocityGlobal,
			velocityRelativeToContact,
			frictionDirection * normalForce * frictionCoefficient,
			deltaTime);
	}
}

void Cloth::ApplyRestitution(QVector3D& velocity, const QVector3D& normal)
{
	// speed scalar, only consider along the normal direction
	float speedIntoCollider = QVector3D::dotProduct(velocity, -normal);

	if (speedIntoCollider < 0)
		// velocity is already moving away from collider
		return;

	// bouncy velocity along the normal direction
	velocity += normal *
		// at 0 restitution, stop velocity along the normal direction
		// at 1 restitution, reflect velocity through the normal direction
		(1.0f + colliderRestitution) *		
		speedIntoCollider;
}

void Cloth::ApplyDeceleration(
	QVector3D& velocityGlobal,
	const QVector3D& velocityRelativeToContact,
	const QVector3D& force,
	double deltaTime)
{
	float dotProduct = QVector3D::dotProduct
	(
		velocityRelativeToContact,
		force
	);
	if (dotProduct < 0)
	{
		// moving in opposing directions, can apply some decceleration
		// ensure decceleration doesn't start accelerating in opposite direction
		float forceScalar = force.length();
		float deccelerationScalar = forceScalar * deltaTime / VertexMass();
		QVector3D decceleration = force * deltaTime / VertexMass();
		float maximumDecceleration = abs(dotProduct / forceScalar);
		if (maximumDecceleration < deccelerationScalar)
			decceleration *= maximumDecceleration / deccelerationScalar;
		velocityGlobal += decceleration;
	}
}

void Cloth::ApplyWindForce(size_t vertexIndex, QVector3D& force)
{
	// normal of this vertex
	const QVector3D& normal =
		representingMesh->normals[representingMesh->faceNormals[vertexIndex]];

	float drag = QVector3D::dotProduct(normal, windPressureDirection * windPressureScalar);
	force += drag * normal
		// this is an extremely rough estimate for surface area for the wind
		/ velocityField.size();
}

bool Cloth::CheckCollision(
	QVector3D& vertexPosition,
	QVector3D& velocity,
	float integratedDistance,
	QVector3D& outNormal,
	QVector3D& outContactPointVelocity)
{
	switch (collideAgainst)
	{
	case Cloth::CollideAgainst::none:
		return false;

	case Cloth::CollideAgainst::plane:
	{
		// infinite plane at (0, 0, 0) with normal (0, 1, 0)
		if (vertexPosition.y() < collidePlaneOriginY)
		{
			// collided
			// move out of collider
			vertexPosition.setY(collidePlaneOriginY);
			outNormal = COLLIDE_PLANE_NORMAL;
			outContactPointVelocity = QVector3D();
			return true;
		}
		// no collision
		else
			return false;
	}

	case Cloth::CollideAgainst::sphere:
	{
		// infinite plane at (0, 0, 0) with normal (0, 1, 0)
		if (vertexPosition.y() < collidePlaneOriginY)
		{
			// collided with plane (floor)
			// move out of collider
			vertexPosition.setY(collidePlaneOriginY);
			outNormal = COLLIDE_PLANE_NORMAL;
			outContactPointVelocity = QVector3D(0, 0, 0);
			return true;
		}

		// AND
		// sphere at (0, 1, 0) with radius 1

		// since velocity is normalized below,
		// these parameters will be equal to distance travelled forward past
		// the ray origin (i.e. vertexPosition)
		float enterRayParameter = 0.0f, exitRayParameter = 0.0f;
		// velocity is normalized to make parameters equal to exact distance
		QVector3D vertexDirection = velocity.normalized();
		// remember, vertex has moved after integration
		// we detecting if it has hit sphere duration that move
		if (RaycastAgainstSphere(
			vertexPosition,
			vertexDirection,
			collideSphereOrigin,
			collideSphereRadius,
			enterRayParameter,
			exitRayParameter) &&
			// vertex has gone past the entrance
			enterRayParameter < 0 &&
			// vertex was not already past sphere before integration
			-integratedDistance < exitRayParameter)
		{
			// closest sphere entrance method
			{
				// move out of collider
				float parameter = abs(enterRayParameter) < abs(exitRayParameter) ?
					enterRayParameter : exitRayParameter;

				// point at closest entrance/exit to sphere
				vertexPosition = vertexPosition + vertexDirection * parameter;

				QVector3D sphereNormal = (vertexPosition - collideSphereOrigin).normalized();
				outNormal = sphereNormal;
				outContactPointVelocity = GetVelocityOnSphere(vertexPosition);
				return true;
			}
		}
		// no collision
		else
			return false;
	}

	default:
		return false;
	}
	// Emergency return
	return false;
}

bool Cloth::RaycastAgainstSphere(
	QVector3D rayOrigin, QVector3D rayDirection,
	QVector3D sphereOrigin, float sphereRadius,
	float& outEnterRayParameter, float& outExitRayParameter)
{
	// parameter that gives the closest point on the ray to the sphere
	float closestRayParameter = QVector3D::dotProduct
	(
		sphereOrigin - rayOrigin,
		rayDirection
	);
	// closest point on the ray to the sphere
	QVector3D closestPointOnRay = rayOrigin + rayDirection * closestRayParameter;
	// closest distance of the ray to the sphere
	float closestRayToSphere = (sphereOrigin - closestPointOnRay).length();
	// squared extensions to the parameter to find the solutions
	float squaredExtension = sphereRadius * sphereRadius - closestRayToSphere * closestRayToSphere;
	if (0 <= squaredExtension)
	{
		float extension = sqrt(squaredExtension);
		// real solution
		outEnterRayParameter = closestRayParameter - extension;
		outExitRayParameter = closestRayParameter + extension;
		return true;
	}
	else
		// no intersection
		return false;
}

QVector3D Cloth::GetVelocityOnSphere(const QVector3D& point)
{
	QVector3D sphereToPoint = point - collideSphereOrigin;
	QVector3D pointForceDirection = QVector3D::crossProduct
	(		
		collideSphereAxisOfRotation,
		sphereToPoint
	);

	if (0 < pointForceDirection.length())
	{
		float speed = collideSphereAngularVelocity * sphereToPoint.length();
		return pointForceDirection.normalized() * speed;
	}
	else
		// point is in-line with axis of rotation, has 0 velocity
		return QVector3D();
}

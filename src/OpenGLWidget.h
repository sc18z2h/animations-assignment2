/*
===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
*/

#pragma once

#include "AdaptiveOpenGL.h"

#include <QGLWidget>
#include <QMouseEvent>
#include <QKeyEvent>
#include <QWheelEvent>
#include <QHoverEvent>
#include <QVector3D>
#include <QTimer>
#include <QElapsedTimer>
#include <QString>
#include <QOpenGLDebugLogger>

#include "Mesh.h"
#include "Cloth.h"

class OpenGLWidget : public QGLWidget
{ // class GeometricWidget
Q_OBJECT
public:

	Cloth cloth;
	Mesh mesh;

	bool isVerticesDraggable;

	bool isSimulationPlaying;

	bool isRecording;

	// constructor
	OpenGLWidget(QWidget *parent);

	// destructor
	~OpenGLWidget();

	void LoadWavefrontModel();

	void SaveWavefrontModel();

	void LoadTextureImage();

	void StepSimulation();

	// either starts recording or stops recording, depending on isRecording state
	void ToggleRecord();

signals:

	void OnMeshChanged(Mesh& newMesh);

	void OnIsTextureEnabledChanged(bool isTextureEnabled);

public slots:
			
protected:

	// this timer updates the paintGL method
	QTimer timer;
	// used to calculate the delta time between frames
	QElapsedTimer elapsedTimer;
	// time on elapsedTimer on the previous frame
	qint64 lastElapsedTime;

	QVector3D cameraPosition;
	QVector3D cameraRotation;

	float scale;
	int lastMouseX, lastMouseY;

	// asynchronously logs OpenGL's error messages
	QOpenGLDebugLogger logger;

	// the vertex being dragged by user's mouse, -1 := no drag
	int draggedVertexIndex;

	// is draggedVertexIndex fixed before dragging started?
	bool isDraggedVertexFixed;

	QString importMessage;

	GLUquadric* sphereQuadratic;
	float sphereRotationAngle;
	
	// gif animation recording
	uint32_t
		recordWidth,
		recordHeight;
	// RGBA values of every pixel of animation frame, in that order
	std::vector<uint8_t> recordData;
	// delay values of each animation frame
	std::vector<uint32_t> recordDelays;
	// the current frame we are recording
	unsigned int recordFrame;

	// mechanism for reducing the recording fps
	// skip recording every n frames
	unsigned int numberOfFramesToSkip;
	// how many frames we have skipped already
	unsigned int framesToSkipLeft;

	// called when OpenGL context is set up
	void initializeGL() override;
	// called every time the widget is resized
	void resizeGL(int w, int h) override;
	// called every time the widget needs painting
	void paintGL() override;

	QQuaternion GetCameraQuaternion() const;
	QString GetRecordingFileName() const;
	// data size of 1 frame in bytes
	size_t GetSizeOfRecordingFrame();
	
	// mouse-handling
	virtual void mousePressEvent(QMouseEvent *event) override;
	virtual void mouseMoveEvent(QMouseEvent *event) override;
	virtual void mouseReleaseEvent(QMouseEvent* event) override;

	// key-handling
	virtual void keyPressEvent(QKeyEvent* event) override;
	virtual void keyReleaseEvent(QKeyEvent* event) override;

	// scroll-wheel-handling
	virtual void wheelEvent(QWheelEvent* event) override;
};

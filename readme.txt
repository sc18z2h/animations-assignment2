===========================
 COMP5823M Animation and Simulation
 Assignment 2: Cloth Simulation
 Zecheng Hu
 sc18z2h
 December 2021
===========================
===========================
compile instructions (windows):

qmake -project QT+=opengl LIBS+=-lopengl32-lglu32
qmake
make

Alternatively, open assignment2.pro in Qt Studio.
===========================
compile instructions (linux):

qmake -project QT+=opengl LIB+=-lGLU
qmake
make
===========================
Third-party libraries:
gif.h by Charlie Tangora. For creating .gif recordings.
